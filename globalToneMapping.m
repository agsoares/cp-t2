function o = globalToneMapping(i)
    a = 0.18;
    
    counter = [0.0, 0.0, 0.0];
    for y = 1:size(i, 1)
        for x = 1:size(i, 2)
            for c = 1:3
                counter(c) = counter(c)+log(0.18 + i(y,x,c));
            end
        end
    end
    lw = exp(counter).*(1/size(i, 1)*size(i, 2));
    l = zeros(size(i,1), size(i,2), 3, 'double');
    for y = 1:size(i, 1)
        for x = 1:size(i, 2)
            for c = 1:3
                l(y,x,c) = (a/lw(c))*i(y,x,c);
            end
        end
    end
    ld = zeros(size(i,1), size(i,2), 3, 'double');
    for y = 1:size(i, 1)
        for x = 1:size(i, 2)
            for c = 1:3
                ld(y,x,c) = (ld(y,x,c)/(1+ld(y,x,c)));
            end
        end
    end
    o = ld;
end

