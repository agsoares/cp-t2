files = {'office_1.jpg', 'office_2.jpg', 'office_3.jpg', ...
         'office_4.jpg', 'office_5.jpg', 'office_6.jpg'};
expTimes = [0.0333, 0.1000, 0.3333, 0.5000, 1.3000, 4.0000];


hdr = makehdr(files, 'RelativeExposure', expTimes ./ expTimes(1));
rgb = tonemap(hdr);
hdrwrite(hdr, 'Task5_hdr.hdr');

figure; imshow(rgb);

imwrite(rgb, 'Task5_ldr.jpg')