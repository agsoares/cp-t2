curve;

files = {'office_1.jpg', 'office_2.jpg', 'office_3.jpg', ...
         'office_4.jpg', 'office_5.jpg', 'office_6.jpg'};
expTimes = [0.0333, 0.1000, 0.3333, 0.5000, 1.3000, 4.0000];


images = cell(length(files),1);
A = 1:6;%length(files);
%Gamma expansion
for i=A
    im = imread(files{i});
    %figure; imshow(im);
    im = (double(im)./255).^2.2.*255;
    %{
    for y = 1:size(im, 1)
       for x = 1:size(im, 2)
           for c = 1:3
               im(y,x,c) = (double(im(y,x,c))/255)^2.2*255;
           end
       end
    end
    %}
    images{i} = im;
    %figure; imshow(im);
end

for i=A
   im = images{i};
   for y = 1:size(im, 1)
       for x = 1:size(im, 2)
           for c = 1:3
               index = int8(im(y,x,c));
               im(y,x,c) = exp(C(index+1, c) - log(expTimes(i)));
           end
       end
   end
   images{i} = im;
end

hdr = zeros(size(images{i},1), size(images{i},2), 3, 'double');
for y = 1:size(hdr, 1)
   for x = 1:size(hdr, 2)
       for c = 1:3
           count = 0;
           val = 0;
           for i=A
               val = val + images{i}(y,x,c);
               count = count+1;
           end
           if (count > 0)
                hdr(y,x,c) = val/count;
           end
       end
   end
end

rgb = tonemap(hdr);
hdrwrite(hdr, 'Task4_hdr.hdr');

figure; imshow(rgb);
imwrite(rgb, 'Task4_ldr.jpg')

